import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { FormBuilder } from '@angular/forms';
import { CrudHttpService } from '../../../../services/crud-http.service';

@Component({
  selector: 'ngx-dialog-encrypt',
  templateUrl: 'dialog-encrypt.component.html',
  styleUrls: ['dialog-encrypt.component.scss'],
})
export class DialogEncryptComponent implements OnInit{

  context: string;
  _id: any;
  decryptData: any;

  constructor(protected ref: NbDialogRef<DialogEncryptComponent>, private crudHttpService: CrudHttpService, private formBuilder: FormBuilder) {}
  
  ngOnInit(){
  }

  cancel():void {
    this.ref.close();
  }
}
