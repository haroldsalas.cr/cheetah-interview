import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">
      Created by Harold S.S. 2022
    </span>
  `,
})
export class FooterComponent {
}
