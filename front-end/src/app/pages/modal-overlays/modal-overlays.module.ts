import { NgModule } from '@angular/core';
import {
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDialogModule,
  NbDialogRef,
  NbInputModule,
  NbPopoverModule,
  NbSelectModule,
  NbTabsetModule,
  NbTooltipModule,
  NbWindowModule,
} from '@nebular/theme';

// modules
import { ThemeModule } from '../../@theme/theme.module';
import { ModalOverlaysRoutingModule } from './modal-overlays-routing.module';

// components
import { ModalOverlaysComponent } from './modal-overlays.component';
import { DialogNamePromptComponent } from './dialog/dialog-name-prompt/dialog-name-prompt.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogEncryptComponent } from './dialog/dialog-encrypt/dialog-encrypt.component';


const COMPONENTS = [
  ModalOverlaysComponent,
  DialogNamePromptComponent,
  DialogEncryptComponent,
];

const ENTRY_COMPONENTS = [
  DialogNamePromptComponent,
  DialogEncryptComponent,
];

const MODULES = [
  FormsModule,
  ReactiveFormsModule,
  ThemeModule,
  ModalOverlaysRoutingModule,
  NbDialogModule.forChild(),
  NbWindowModule.forChild(),
  NbCardModule,
  NbCheckboxModule,
  NbTabsetModule,
  NbPopoverModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbTooltipModule,
  
];

const SERVICES = [
];

@NgModule({
  imports: [
    ...MODULES,

  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class ModalOverlaysModule {
}
