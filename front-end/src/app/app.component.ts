/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { SeoService } from './@core/utils/seo.service';
import { CrudHttpService } from './services/crud-http.service';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  recordList : any = [];

  constructor(private analytics: AnalyticsService, private seoService: SeoService, private crudHttpService: CrudHttpService) {
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
    this.seoService.trackCanonicalChanges();
  }

  showAll(){
    this.crudHttpService.showAll().subscribe((response) => {
      this.recordList = response;
    },(error => {}));
  }

  createRecord(data:any){
    this.crudHttpService.create(data).subscribe((response) => {
      this.showAll();
    },(error => {}));
  }

  updateRecord(data:any){
    this.crudHttpService.update(data.id,data).subscribe((response)=>{
      this.showAll();
    },(error => {}));
  }

  deleteRecord(id:any){
    this.crudHttpService.delete(id).subscribe((response)=>{
      this.showAll();
    },(error => {}));
  }
}
