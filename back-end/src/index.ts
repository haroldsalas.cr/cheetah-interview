import express from 'express';
import { json } from 'body-parser';
import mongoose  from 'mongoose';
import dotenv from 'dotenv';

import { createRouter } from './routes/createRecord';
import { deleteRouter } from './routes/deleteRecord';
import { showRouter } from './routes/showRecord';
import { updateRouter } from './routes/updateRecord';


dotenv.config();

const app = express();
app.use(json());

app.use(createRouter);
app.use(deleteRouter);
app.use(showRouter);
app.use(updateRouter);

const start = async () => {
    if (!process.env.MONGO_URI){
        throw new Error('MONGO_URI must be defined!');
    }

    try {
        await mongoose.connect(process.env.MONGO_URI);
        console.log('Connected to MongoDB...')
    } catch (err) {
        console.error(err);
    }

    app.listen(3000, () => {
        console.log("Listen on port 3000!");
    })
}

start();

