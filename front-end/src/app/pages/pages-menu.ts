import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  
  {
    title: 'Laptops Info',
    icon: 'edit-2-outline',
    link: '/main/employeesLaptops/',
  },
  
];
