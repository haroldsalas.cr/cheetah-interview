import { Component } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'ngx-dialog-name-prompt',
  templateUrl: 'dialog-name-prompt.component.html',
  styleUrls: ['dialog-name-prompt.component.scss'],
})
export class DialogNamePromptComponent {

  checkoutForm : FormGroup;

  constructor(protected ref: NbDialogRef<DialogNamePromptComponent>, private formBuilder: FormBuilder) {
    this.crearForm();
  }

  crearForm() {
    this.checkoutForm = this.formBuilder.group({
      brand: ['', Validators.required],
      model: ['', Validators.required],
      serial: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
    })
  }

  cancel():void {
    this.ref.close();
  }

  onSubmit():void {
    const value = this.checkoutForm.value;
    
    if(!this.checkoutForm.valid){
      return alert("The information is invalid");
    }
    this.ref.close(value);
  }
}
